<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?php echo $data['titulo']; ?></h1>

</div>

<!-- Content Row -->

<div class="row">
    <div class="col-12">
        <div class="card shadow mb-4">
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary"><?php echo $data['div_titulo']; ?></h6>                                    
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <form action="./?sec=formulario" method="post">
                    <!--<form method="get">-->
                    <input type="hidden" name="sec" value="formulario" />
                    <div class="mb-3">
                        <label for="labelNombre">Nombre</label>
                        <input class="form-control" id="labelNombre" type="text" name="nombre" placeholder="Inserte su nombre" value="">
                        <p class="text-danger"><small>Texto de error.</small></p>                                                    
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1">Correo electrónico</label>
                        <input class="form-control" id="exampleFormControlInput1" type="email" name="email" placeholder="name@example.com" value="">                        
                        <p class="text-danger"><small>Texto de error.</small></p>                                                    
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlSelect1">Ejemplo select</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="selectnormal">
                            <option value="1">Uno</option>
                            <option value="2">Dos</option>
                            <option value="3">Tres</option>
                            <option value="4">Cuatro</option>
                            <option value="5">Cinco</option>
                        </select>
                        <p class="text-danger"><small>Texto de error.</small></p>                                                    
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlSelect2">Ejemplo multiple select</label>
                        <select class="form-control" id="exampleFormControlSelect2" multiple="" name="selectmultiple[]">
                            <option value="1">Uno</option>
                            <option value="2">Dos</option>
                            <option value="3">Tres</option>
                            <option value="4">Cuatro</option>
                            <option value="5">Cinco</option>
                        </select>
                        <p class="text-danger"><small>Texto de error.</small></p>       
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlSelect2">Ejemplo checkbox</label>  
                        <div class="row">
                            <div class="col-sm">
                                <div class="form-check">
                                    <input class="form-check-input" id="flexCheckDefault" type="checkbox" value="a" name="opcions[]">
                                    <label class="form-check-label" for="flexCheckDefault">Default checkbox</label>
                                </div>
                                <p class="text-danger"><small>Texto de error.</small></p>
                            </div>
                            <div class="col-sm">
                                <div class="form-check">
                                    <input class="form-check-input" id="flexCheckChecked" type="checkbox" value="b" checked" name="opcions[]" checked>
                                    <label class="form-check-label" for="flexCheckChecked">Checked checkbox</label>
                                </div>
                                <p class="text-danger"><small>Texto de error.</small></p>
                            </div>
                            <div class="col-sm">
                                <div class="form-check">
                                    <input class="form-check-input" id="flexCheckDisabled" type="checkbox" value="c" disabled name="opcions[]">
                                    <label class="form-check-label" for="flexCheckDisabled">Disabled checkbox</label>
                                </div>
                                <p class="text-danger"><small>Texto de error.</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1">Ejemplo textarea</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" name="textarea" rows="3"><?php echo isset($data['sanitized_post']['textarea']) ? $data['sanitized_post']['textarea'] : ''; ?></textarea>
                        <p class="text-danger"><small>Texto de error.</small></p>
                    </div>
                    <div class="mb-3">
                        <label for="summernote">Ejemplo textarea enriquecido</label>
                        <textarea class="form-control" id="summernote" name="summernote"><?php echo isset($data['sanitized_post']['summernote']) ? $data['sanitized_post']['summernote'] : ''; ?></textarea>
                        <p class="text-danger"><small>Texto de error.</small></p>
                    </div>
                    <div class="mb-3">
                        <input type="submit" value="Enviar" name="submit" class="btn btn-primary"/>
                    </div>
                </form>
            </div>
        </div>
    </div>                        
</div>

