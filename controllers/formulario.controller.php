<?php
$data['titulo'] = "Plantilla formulario";
$data["div_titulo"] = "Formulario";

$data['js'] = array("vendor/summernote/summernote-bs4.min.js", "assets/pages/js/formulario.view.js");

include 'views/templates/header.php';
include 'views/formulario.view.php';
include 'views/templates/footer.php';